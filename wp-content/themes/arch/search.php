<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content">

			<?php
				$arch_blog_sidebar_type = esc_attr( get_option('arch_blog_sidebar_type') );
				$arch_blog_pnav = esc_attr( get_option('arch_blog_pnav') );
				$arch_search_header = esc_attr( get_option('arch_search_header') );

				if($arch_blog_sidebar_type == "Left") :
					$arch_blog_class_main = "left-content right";
					$arch_blog_class_sidebar = "sidebar left";
				else :
					$arch_blog_class_main = "left-content";
					$arch_blog_class_sidebar = "sidebar";
				endif;
			?>

			<h2 class="ptitle">
				<?php 
					if(!empty($arch_search_header)) : echo $arch_search_header; 
					else : _e( 'Search Results', ENGINE_THEME_NAME ); 
					endif; 
				?>
			</h2>
			
			<div class="content-inside clear">

				<!--LEFT CONTENT-->
				<div class="<?php echo $arch_blog_class_main; ?>">

					<!--BLOG LIST-->
					<section class="blist clear">

						<?php
							if ( have_posts() ) :

								for($i = 1; have_posts(); $i++) { 							
									the_post();			
									$columns = 2;	
									$class = '';
									$class .= ($i % $columns == 0) ? ' last' : '';

									get_template_part( 'loop', 'entry' );
								}	

						?>	
					</section>

					<!--PAGE NAVIGATION-->
					<?php
						if($arch_blog_pnav == "Next Previous Link") : next_previous_link();
						else : pagination();
						endif;
						wp_link_pages();
					?>

					<?php 
						else :
							?><div class="search-error"><?php _e( 'No Results Found on your Query', ENGINE_THEME_NAME ); ?></div><?php
						endif;	
					?>

				</div>

				<!--SIDEBAR-->
				<div class="<?php echo $arch_blog_class_sidebar; ?>">
					<?php
						if($sidebar_choice == "Default") : get_sidebar( 'blog' );
						else : get_custom_sidebar(); 
						endif;
					?>
				</div>	
			</div>
		</div>
	</div>

<?php get_footer(); ?>