<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content project-inside">
			<?php $arch_term = $wp_query->queried_object; ?>
			<h2 class="ptitle"><?php echo $arch_term->name; ?></h2>
			<?php get_projects_category(); ?>
		</div>
	</div>

	<!--PROJECTS-->
	<div class="project-page load-item clear">
		<!--PROJECT LIST-->
		<div class="horScroll">
			<section class="project-list clear">
				<?php
					$arch_project_order = esc_attr( get_option('arch_album_order') );
					$arch_project_orderby = esc_attr( get_option('arch_album_orderby') );

					$args = array( 
						'post_type' => 'projects', 
						'taxonomy' => 'project_categories', 
						'orderby' => $arch_project_orderby, 
						'order' => $arch_project_order, 
						'posts_per_page' => -1, 
						'term' => $arch_term->slug 
					);
					$wp_query = new WP_Query( $args );	

					while ($wp_query->have_posts()) : $wp_query->the_post(); 		
						get_template_part( 'loop', 'projects' );							
					endwhile;
				?>	
			</section>
		</div>
	</div>

<?php get_footer(); ?>