<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content">

			<div class="content-inside clear">

				<?php
					$arch_blog_sidebar_type = esc_attr( get_option('arch_blog_sidebar_type') );
					$arch_blog_pnav = esc_attr( get_option('arch_blog_pnav') );

					if($arch_blog_sidebar_type == "Left") :
						$arch_blog_class_main = "left-content right";
						$arch_blog_class_sidebar = "sidebar left";
					else :
						$arch_blog_class_main = "left-content";
						$arch_blog_class_sidebar = "sidebar";
					endif;
				?>

				<!--LEFT CONTENT-->
				<div class="<?php echo $arch_blog_class_main; ?>">

					<!--BLOG LIST-->
					<section class="blist clear">

						<?php
							get_home_pagination();

							$args = array( 'post_type' => 'post', 'posts_per_page' => get_option('arch_num_blog'), 'paged' => $paged );
							$wp_query = new WP_Query( $args );	

							if ( $wp_query->have_posts() ) :

								for($i = 1; $wp_query->have_posts(); $i++) { 							
									$wp_query->the_post();			
									$columns = 2;	
									$class = '';
									$class .= ($i % $columns == 0) ? ' last' : '';
									
									get_template_part( 'loop', 'entry' );

								}

							else :
								_e( 'You should add a record for your Blog Post', ENGINE_THEME_NAME );
							endif;	

						?>	
					</section>

					<!--PAGE NAVIGATION-->
					<?php
						if($arch_blog_pnav == "Next Previous Link") : next_previous_link();
						else : pagination();
						endif;
						wp_link_pages();
					?>

				</div>

				<!--SIDEBAR-->
				<div class="<?php echo $arch_blog_class_sidebar; ?>">
					<?php
						if($sidebar_choice == "Default") : get_sidebar( 'blog' );
						else : get_custom_sidebar(); 
						endif;
					?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>