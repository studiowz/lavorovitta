<!DOCTYPE html>
<html>
<head>
    <title>.:: Larovovitta ::.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Le styles -->
    <style type="text/css">
    .vertical-center {
        min-height: 100%;
        min-height: 100vh;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }
    </style>
    <link href="../assets/css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
</head>
<body>

<div class="vertical-center">
    <a href="mailto:contato@lavorovitta.com.br">
        <img src="images/logo.jpg" class="center-block img-responsive"/>
    </a>
</div>

<script src="http://code.jquery.com/jquery.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>