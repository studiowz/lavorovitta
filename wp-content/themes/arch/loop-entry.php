<?php 
	global $class; 

	$arch_continue_reading = esc_attr( get_option('arch_continue_reading') );
	$arch_display_excerpt = esc_attr( get_option('arch_display_excerpt') );
?>

<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class($class) ); ?>>
	<div class="image-hover">
		<a href="<?php the_permalink(); ?>">
			<?php
				if ( post_password_required() ) : ?>
					<img src="<?php echo get_template_directory_uri(); ?>/img/password-protect-blog.jpg" alt="" title="" /><?php
				else :
					if ( has_post_thumbnail() ) : 
						the_post_thumbnail('blog-large', array('title' => ''.get_the_title().'')); 
					endif;
				endif;			
			?>					
		</a>
		<a href="<?php the_permalink(); ?>" class="kreading">
			<?php 
				if(!empty($arch_continue_reading)) : echo $arch_continue_reading;
				else : _e( 'Keep reading', ENGINE_THEME_NAME ); 
				endif; 
			?><span>&nbsp;</span>
		</a>
	</div>
	<div class="blog-details">
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php if(!empty($arch_display_excerpt)) : the_excerpt(); endif; ?>
	</div>
	<div class="single-details list">
		<ul>
			<li class="date"><?php the_time(get_option('date_format')); ?></li>
			<li class="user"><?php the_author_meta('display_name'); ?></li>
			<li class="bubble">
				<a href="<?php the_permalink(); ?>#comments">
					<?php comments_number('0', '1', '%'); echo "&nbsp;"; _e( 'Comments', ENGINE_THEME_NAME ); ?>
				</a>
			</li>
		</ul>
	</div>
</div>		