<?php
/*
Template Name: About	
*/	
?>

<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content">

			<?php 
				get_theme_page_title(); 

				$arch_services_hide = esc_attr( get_option('arch_services_hide') );
				$arch_members_hide = esc_attr( get_option('arch_members_hide') );
				$arch_member_header = esc_attr( get_option('arch_member_header') );
				$arch_services_order = esc_attr( get_option('arch_services_order') );
				$arch_services_orderby = esc_attr( get_option('arch_services_orderby') );
				$arch_num_services = esc_attr( get_option('arch_num_services') );
				$arch_member_order = esc_attr( get_option('arch_member_order') );
				$arch_member_orderby = esc_attr( get_option('arch_member_orderby') );
				$arch_num_member = esc_attr( get_option('arch_num_member') );
				
				//Services
				if(empty($arch_services_hide)) : ?>	
					<div class="services">
						<ul class="clear">
							<?php
								$args = array( 
									'post_type' => 'services', 
									'orderby' => $arch_services_orderby, 
									'order' => $arch_services_order, 
									'posts_per_page' => $arch_num_services 
								);	
								$wp_query = new WP_Query( $args );	
								
								for($i = 1; $wp_query->have_posts(); $i++) { 							
									$wp_query->the_post();			
									$columns = 3;	
									$class = '';
									$class .= ($i % $columns == 0) ? ' last' : '';
									?>
									<li id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class($class) ); ?>>
										<?php
											if ( has_post_thumbnail() ) :
												the_post_thumbnail('services-thumb', array('title' => ''.get_the_title().''));
											endif;
										?>
										<h3><?php the_title(); ?></h3>
										<?php the_excerpt(); ?>
									</li><?php
								}
								wp_reset_query();	
							?>
						</ul>				
					</div><?php
				endif;

				//Our Team
				if(empty($arch_members_hide)) : ?>
					<div class="team">
						<h2 class="ptitle">
							<?php 
								if(!empty($arch_member_header)) : echo $arch_member_header; 
								else : _e( 'Who is behind Arch', ENGINE_THEME_NAME ); 
								endif;
							?>
						</h2>
						<div class="team-list clear">
							<?php
								$args = array( 
									'post_type' => 'ourteam', 
									'orderby' => $arch_member_orderby, 
									'order' => $arch_member_order, 
									'posts_per_page' => $arch_num_member 
								);			
								$wp_query = new WP_Query( $args );	

								for($i = 1; $wp_query->have_posts(); $i++) { 							
									$wp_query->the_post();			
									$columns = 3;	
									$class = 'member';
									$class .= ($i % $columns == 0) ? ' last' : '';

									$arch_facebook = esc_attr( get_post_meta( $post->ID, "facebook", true ) );
									$arch_twitter = esc_attr( get_post_meta( $post->ID, "twitter", true ) );
									$arch_tumblr = esc_attr( get_post_meta( $post->ID, "tumblr", true ) );
									$arch_pinterest = esc_attr( get_post_meta( $post->ID, "pinterest", true ) );
									$arch_behance = esc_attr( get_post_meta( $post->ID, "behance", true ) );
									$arch_dribbble = esc_attr( get_post_meta( $post->ID, "dribbble", true ) );
									$arch_youtube = esc_url( get_post_meta( $post->ID, "youtube", true ) );
									$arch_position = esc_attr( get_post_meta( $post->ID, "position", true ) );

									?>
									<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class($class) ); ?>>
										<div class="thumbs-animate">
											<?php
												if ( has_post_thumbnail() ) : 
													the_post_thumbnail(array(287,250), array('title' => ''.get_the_title().''));
												endif;
											?>
											<div class="team-social clear">
												<ul>
													<?php
														if(!empty($arch_facebook)) : ?>
															<li>
																<a href="http://facebook.com/<?php echo $arch_facebook; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_twitter)) : ?>
															<li>
																<a href="http://twitter.com/<?php echo $arch_twitter; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/twitter.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_tumblr)) : ?>
															<li>
																<a href="http://<?php echo $arch_tumblr; ?>.tumblr.com" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/tumbler.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_pinterest)) : ?>
															<li>
																<a href="http://pinterest.com/<?php echo $arch_pinterest; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/pinterest.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_behance)) : ?>
															<li>
																<a href="http://behance.net/<?php echo $arch_behance; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/behance.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_dribbble)) : ?>
															<li>
																<a href="http://dribbble.com/<?php echo $arch_dribbble; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/dribbble.png" alt="" title="" />
																</a>
															</li><?php
														endif;
														if(!empty($arch_youtube)) : ?>
															<li>
																<a href="<?php echo $arch_youtube; ?>" title="" target="_blank">
																	<img src="<?php echo get_template_directory_uri(); ?>/img/icons/youtube.png" alt="" title="" />
																</a>
															</li><?php
														endif;
													?>
												</ul>
											</div>
										</div>
										
										<div class="mdetails">
											<h3><?php the_title(); ?></h3>
											<span><?php echo $arch_position; ?>	</span>
										</div>
									</div>
									<?php
								}
								wp_reset_query();	
							?>
						</div>				
					</div><?php
				endif;
			?>
		</div>
	</div>

<?php get_footer(); ?>