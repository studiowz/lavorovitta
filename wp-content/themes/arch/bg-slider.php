<?php
	$arch_slide_title = esc_attr( get_option('arch_slide_title') );
	$arch_arrow_nav = esc_attr( get_option('arch_arrow_nav') );
	$arch_slide_int = esc_attr( get_option('arch_slide_int') );
	$arch_slide_trans = esc_attr( get_option('arch_slide_trans') );
	$arch_slide_speed = esc_attr( get_option('arch_slide_speed') );
	$arch_min_width = esc_attr( get_option('arch_min_width') );
	$arch_min_height = esc_attr( get_option('arch_min_height') );
	$arch_vertical_center = esc_attr( get_option('arch_vertical_center') );
	$arch_horizontal_center = esc_attr( get_option('arch_horizontal_center') );
	$arch_fit_always = esc_attr( get_option('arch_fit_always') );
	$arch_fit_portrait = esc_attr( get_option('arch_fit_portrait') );
	$arch_fit_landscape = esc_attr( get_option('arch_fit_landscape') );
	$arch_bg_order = esc_attr( get_option('arch_bg_order') );
	$arch_bg_orderby = esc_attr( get_option('arch_bg_orderby') );	
	$arch_bg_homepage_slider = esc_attr( get_option('arch_bg_homepage_slider') );
	$arch_bgimage = esc_attr( get_post_meta(@$post->ID, "bgimage", true) );
	$arch_archive_bgimage = esc_attr( get_option('arch_archive_bgimage') );
	$arch_search_bgimage = esc_attr( get_option('arch_search_bgimage') );
	$arch_notfound_bgimage = esc_attr( get_option('arch_notfound_bgimage') );
	$arch_title_link = esc_attr( get_option('arch_title_link') );
	$arch_theme_bgimage = esc_attr( get_option('arch_theme_bgimage') );
	
	if(is_page_template('template-homepage.php')) : ?>
		<!--CAPTION and ARROW-->
		<div class="welcome-arrow load-item">
			<?php
				if(empty($arch_slide_title)) :
					echo "<div id='slidecaption'></div>";
				endif;

				if(empty($arch_arrow_nav)) : ?>
					<ul class="nav clear">
						<li class="sprev"><a id="prevslide">&nbsp;</a></li>
						<li class="snext"><a id="nextslide">&nbsp;</a></li>
					</ul><?php
				endif;
			?>
		</div><?php
	endif;	

	if ( ! function_exists( 'arch_slide_settings' ) ) :
		function arch_slide_settings() {
			if(!empty($arch_slide_int)) : $arch_slide_int_value = $arch_slide_int;
			else : $arch_slide_int_value = "3000";
			endif;

			if(!empty($arch_slide_trans)) : $arch_slide_trans_value = $arch_slide_trans;
			else : $arch_slide_trans_value = "1";
			endif;

			if(!empty($arch_slide_speed)) : $arch_slide_speed_value = $arch_slide_speed;
			else : $arch_slide_speed_value = "700";
			endif;

			if(!empty($arch_min_width)) : $arch_min_width_value = $arch_min_width;
			else : $arch_min_width_value = "0";
			endif;

			if(!empty($arch_min_height)) : $arch_min_height_value = $arch_min_height;
			else : $arch_min_height_value = "0";
			endif;

			if(!empty($arch_vertical_center)) : $arch_vertical_center_value = $arch_vertical_center;
			else : $arch_vertical_center_value = "0";
			endif;

			if(!empty($arch_horizontal_center)) : $arch_horizontal_center_value = $arch_horizontal_center;
			else : $arch_horizontal_center_value = "1";
			endif;

			if(!empty($arch_fit_always)) : $arch_fit_always_value = $arch_fit_always;
			else : $arch_fit_always_value = "0";
			endif;

			if(!empty($arch_fit_portrait)) : $arch_fit_portrait_value = $arch_fit_portrait;
			else : $arch_fit_portrait_value = "1";
			endif;

			if(!empty($arch_fit_landscape)) : $arch_fit_landscape_value = $arch_fit_landscape;
			else : $arch_fit_landscape_value = "1";
			endif;
			?>
				slide_interval: <?php echo $arch_slide_int_value; ?>,
				transition: <?php echo $arch_slide_trans_value; ?>,							
				transition_speed: <?php echo $arch_slide_speed_value; ?>, 								   
				slide_links: 'blank',			
				min_width: <?php echo $arch_min_width_value; ?>,
				min_height: <?php echo $arch_min_height_value; ?>,
				vertical_center: <?php echo $arch_vertical_center_value; ?>,
				horizontal_center: <?php echo $arch_horizontal_center_value; ?>,
				fit_always: <?php echo $arch_fit_always_value; ?>,
				fit_portrait: <?php echo $arch_fit_portrait_value; ?>,
				fit_landscape: <?php echo $arch_fit_landscape_value; ?>,
			<?php
		}
	endif;

	if(!empty($arch_bg_homepage_slider)) :
		if(is_front_page()) :
			$args = array( 
				'post_type' => 'projects', 
				'posts_per_page' => -1, 
				'meta_query' => array( array( 'key' => 'bgslide', 'value' => 'on', 'compare' => '=' ) ) 
			);
			$my_query = new WP_Query( $args );

			if( $my_query->have_posts() ) : ?>
				<script type="text/javascript">	
					(function($) {
					  	"use strict";					  	
					  	jQuery(function($){	

							var image_slides=[];
							<?php
								while($my_query->have_posts()) :
							      $my_query->the_post();

							      $image_query = new WP_Query( array( 
							        	'post_type' => 'attachment', 
							        	'post_status' => 'inherit', 
							        	'order' => $arch_bg_order, 
							        	'orderby' => $arch_bg_orderby, 
							        	'post_mime_type' => 'image', 
							        	'posts_per_page' => -1, 
							        	'post_parent' => get_the_ID() ) 
							      );

						        	while( $image_query->have_posts() ) :
						            $image_query->the_post();
						            $image_attributes = wp_get_attachment_image_src('', 'full');
						            $image_attributes_thumb = wp_get_attachment_image_src('', 'thumbnail');	
						            $cf_image_link = get_post_meta($id, '_cf-image-link', true); 

						           	if(empty($arch_title_link)) : ?>
							            image_slides.push({image : '<?php echo $image_attributes[0] ?>', title : '<a href="<?php if(get_post_meta($id, '_cf-image-link', true)) { echo $cf_image_link; } else { the_permalink(); } ?>"><?php the_title(); ?></a>', thumb : '<?php echo $image_attributes_thumb[0] ?>'})	
							            <?php
						            else : ?>
							            image_slides.push({image : '<?php echo $image_attributes[0] ?>', title : '<?php the_title(); ?>', thumb : '<?php echo $image_attributes_thumb[0] ?>'})	
							            <?php
						            endif;
						         endwhile;
							   endwhile;
							   wp_reset_query();					
							?>		

							$.supersized({			
								<?php arch_slide_settings(); ?>
								slides: image_slides,								
							});
					   });	
					})(jQuery);									    
				</script>
				<?php
			else : arch_temp_bg_image();
			endif;
		else :
			if(is_archive()) :
				if(!empty($arch_archive_bgimage)) : ?>
					<script type="text/javascript">	
						(function($) {
						  	"use strict";
						  	jQuery(function($){			
								$.supersized({	
									<?php arch_slide_settings(); ?>
									slides: [ {image : '<?php echo $arch_archive_bgimage; ?>', title : '<?php the_title(); ?>'} ] 
								});
						    });		
						})(jQuery);
					</script><?php	
				else : arch_default_img_bg(); 
				endif;
			elseif(is_search()) :
				if(!empty($arch_search_bgimage)) : ?>
					<script type="text/javascript">	
						(function($) {
						  	"use strict";
						  	jQuery(function($){			
								$.supersized({	
									<?php arch_slide_settings(); ?>
									slides: [ {image : '<?php echo $arch_search_bgimage; ?>', title : '<?php the_title(); ?>'} ] 
								});
						    });		
						})(jQuery);								    
					</script><?php	
				else : arch_default_img_bg();
				endif;
			elseif(is_404()) :
				if(!empty($arch_notfound_bgimage)) : ?>
					<script type="text/javascript">	
						(function($) {
						  	"use strict";
						  	jQuery(function($){			
								$.supersized({	
									<?php arch_slide_settings(); ?>
									slides: [ {image : '<?php echo $arch_notfound_bgimage; ?>', title : '<?php the_title(); ?>'} ] 
								});
						    });		
						})(jQuery);    
					</script><?php	
				else : arch_default_img_bg(); 
				endif;
			elseif(is_page() || is_single()) :
				if(!empty($arch_bgimage)) : ?>
					<script type="text/javascript">	
						(function($) {
						  	"use strict";
						  	jQuery(function($){			
								$.supersized({
									<?php arch_slide_settings(); ?> 
									slides: [ {image : '<?php echo $arch_bgimage; ?>', title : '<?php the_title(); ?>'} ] 
								});
						    });		
						})(jQuery);									    
					</script><?php	
				else : arch_default_img_bg(); 
				endif;
			else : arch_default_img_bg();
			endif;
		endif;		    
	else :
		$args = array( 
			'post_type' => 'projects', 
			'posts_per_page' => -1, 
			'meta_query' => array( array( 'key' => 'bgslide', 'value' => 'on', 'compare' => '=' ) ) 
		);
		$my_query = new WP_Query( $args );

		if( $my_query->have_posts() ) : ?>
			<script type="text/javascript">	
				(function($) {
				  	"use strict";
				  	jQuery(function($){
						var image_slides=[];
						<?php
							while($my_query->have_posts()) :
						      $my_query->the_post();

						      $image_query = new WP_Query( array( 
						      	'post_type' => 'attachment', 
						      	'post_status' => 'inherit', 
						      	'order' => $arch_bg_order, 
							      'orderby' => $arch_bg_orderby, 
						      	'post_mime_type' => 'image', 
						      	'posts_per_page' => -1, 
						      	'post_parent' => get_the_ID() ) 
						      );

					        	while( $image_query->have_posts() ) :
					            $image_query->the_post();
					            $image_attributes = wp_get_attachment_image_src('', 'full');
					            $image_attributes_thumb = wp_get_attachment_image_src('', 'thumbnail'); 
						        	$cf_image_link = get_post_meta($id, '_cf-image-link', true); 

					            if(empty($arch_title_link)) : ?>
						            image_slides.push({image : '<?php echo $image_attributes[0] ?>', title : '<a href="<?php if(get_post_meta($id, '_cf-image-link', true)) { echo $cf_image_link; } else { the_permalink(); } ?>"><?php the_title(); ?></a>', thumb : '<?php echo $image_attributes_thumb[0] ?>'})	
						            <?php
					            else : ?>
						            image_slides.push({image : '<?php echo $image_attributes[0] ?>', title : '<?php the_title(); ?>', thumb : '<?php echo $image_attributes_thumb[0] ?>'})	
						            <?php
					            endif;				            
						      endwhile;
						   endwhile;
						   wp_reset_query();					
						?>		
						$.supersized({			
							<?php arch_slide_settings(); ?>							
							slides: image_slides,							
						});
				    });	
				})(jQuery);	    
			</script>
			<?php
		else : arch_temp_bg_image();
		endif;
	endif;


	//Functions
	if ( ! function_exists( 'arch_default_img_bg' ) ) :
		function arch_default_img_bg() {
		 	?>
				<script type="text/javascript">	
					(function($) {
					  	"use strict";
					  	jQuery(function($){			
							$.supersized({	
								<?php arch_slide_settings(); ?>
								slides: [ {image : '<?php echo $arch_theme_bgimage; ?>', title : '<?php the_title(); ?>'} ] 
							});
					    });		
					})(jQuery);    
				</script>
			<?php	
		}	
	endif;

	if ( ! function_exists( 'arch_temp_bg_image' ) ) :
		function arch_temp_bg_image() {
			?>
				<script type="text/javascript">	
					(function($) {
					  	"use strict";
					  	jQuery(function($){			
							$.supersized({ 
								<?php arch_slide_settings(); ?>
								slides: [ 
									{image : 'http://wp.themecss.com/Arch/wp-content/uploads/2013/01/2608974320_a4ec241445_o.jpg', title : '<a href="#">This is your temporary background image</a>'}, 
									{image : 'http://wp.themecss.com/Arch/wp-content/uploads/2013/01/home.jpg', title : '<a href="#">Check our documentation on how to set up</a>'} 
								] });
					    });			
					})(jQuery);						    
				</script>
			<?php	
		}
	endif;
?>