<?php
	function arch_options_setup() {
		global $themename, $shortname, $options, $version, $help, $author;

		$version = "1.5.4";	
		$author = "EngineThemes";	
		$themename = "Arch";
		$shortname = "arch";	
		$help = "http://themecss.com/enginethemes/?forum=arch";

		$options = array (

			array( "name" => __( 'Theme Settings', ENGINE_THEME_NAME ), "type" => "top_section"),
			array( "type" => "close"),	

			array( "name" => __( 'General', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-cog"),
					
			array( "type" => "open"),

			array( "name" => __( 'Main', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Responsive Layout', ENGINE_THEME_NAME ),
					"desc" => __( 'Disable Responsive Effect', ENGINE_THEME_NAME ),
					"id" => $shortname."_site_layout",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Header Widget', ENGINE_THEME_NAME ),
					"desc" => __( 'Hide your header widget', ENGINE_THEME_NAME ),
					"id" => $shortname."_header_widget",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Favicon', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your website favicon', ENGINE_THEME_NAME ),
					"id" => $shortname."_favicon",
					"type" => "img_preview",
					"std" => ""),
					
			array( "name" => __( 'Logo', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your website logo', ENGINE_THEME_NAME ),
					"id" => $shortname."_logo",
					"type" => "img_preview",
					"std" => ""),	

			array( "name" => __( 'Map Icon', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your google map icon', ENGINE_THEME_NAME ),
					"id" => $shortname."_map_icon",
					"type" => "img_preview",
					"std" => ""),

			array( "name" => __( 'Footer', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Custom CSS', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your custom CSS tags here', ENGINE_THEME_NAME ),
					"id" => $shortname."_custom_css",
					"type" => "textarea",
					"std" => ""),

			array( "name" => __( 'Google Analytics Code', ENGINE_THEME_NAME ),
					"desc" => __( 'You can paste your Google Analytics or other tracking code in this box', ENGINE_THEME_NAME ),
					"id" => $shortname."_ga_code",
					"type" => "textarea",
					"std" => ""),

			array( "type" => "close"),

			array( "name" => __( 'Background', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-square"),

			array( "name" => __( 'Fallback', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Background Fallback', ENGINE_THEME_NAME ),
					"desc" => __( 'Upload your background image fallback when page and post background is empty', ENGINE_THEME_NAME ),
					"id" => $shortname."_theme_bgimage",
					"type" => "upload",
					"std" => ""),

			array( "name" => __( 'Archive Background Image', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your archive page background image', ENGINE_THEME_NAME ),
					"id" => $shortname."_archive_bgimage",
					"type" => "upload",
					"std" => ""),

			array( "name" => __( 'Search Background Image', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your search page background image', ENGINE_THEME_NAME ),
					"id" => $shortname."_search_bgimage",
					"type" => "upload",
					"std" => ""),

			array( "name" => __( '404 Background Image', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your 404 not found page background image', ENGINE_THEME_NAME ),
					"id" => $shortname."_notfound_bgimage",
					"type" => "upload",
					"std" => ""),

			array( "name" => __( 'Slider', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Frontpage Slider', ENGINE_THEME_NAME ),
					"desc" => __( 'check this box if you want your slider display in homepage only', ENGINE_THEME_NAME ),
					"id" => $shortname."_bg_homepage_slider",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Arrow navigation', ENGINE_THEME_NAME ),
					"desc" => __( 'Disable the arrow navigation of the slider', ENGINE_THEME_NAME ),
					"id" => $shortname."_arrow_nav",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Slide Title', ENGINE_THEME_NAME ),
					"desc" => __( 'Check the box if you want to hide slide title', ENGINE_THEME_NAME ),
					"id" => $shortname."_slide_title",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Title Link', ENGINE_THEME_NAME ),
					"desc" => __( 'check this box if you want your slider title unlink', ENGINE_THEME_NAME ),
					"id" => $shortname."_title_link",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Image Ordering', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your ordering style for your background images', ENGINE_THEME_NAME ),
					"id" => $shortname."_bg_order",
					"type" => "select",
					"options" => array("ASC", "DESC"),
					"std" => ""),

			array( "name" => __( 'Image Orderby', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your orderby parameter for your background images', ENGINE_THEME_NAME ),
					"id" => $shortname."_bg_orderby",
					"type" => "select",
					"options" => array("ID", "name", "date", "rand"),
					"std" => ""),

			array( "name" => __( 'Slide Interval', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter Length between transitions', ENGINE_THEME_NAME ),
					"id" => $shortname."_slide_int",
					"type" => "text",
					"std" => "3000"),

			array( "name" => __( 'Transition', ENGINE_THEME_NAME ),
					"desc" => "Enter transitions effect you want (0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left)",
					"id" => $shortname."_slide_trans",
					"type" => "select",
					"options" => array("0", "1", "2", "3", "4", "5", "6", "7"),
					"std" => ""),

			array( "name" => __( 'Transition Speed', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter transition speed for background slider', ENGINE_THEME_NAME ),
					"id" => $shortname."_slide_speed",
					"type" => "text",
					"std" => "700"),

			array( "name" => __( 'Minimum Width', ENGINE_THEME_NAME ),
					"desc" => __( 'Minimum width allowed in pixels', ENGINE_THEME_NAME ),
					"id" => $shortname."_min_width",
					"type" => "text",
					"std" => "0"),

			array( "name" => __( 'Minimum Height', ENGINE_THEME_NAME ),
					"desc" => __( 'Minimum height allowed in pixels', ENGINE_THEME_NAME ),
					"id" => $shortname."_min_height",
					"type" => "text",
					"std" => "0"),

			array( "name" => __( 'Vertical Center', ENGINE_THEME_NAME ),
					"desc" => __( 'Vertically center background', ENGINE_THEME_NAME ),
					"id" => $shortname."_vertical_center",
					"type" => "select",
					"options" => array("0", "1"),
					"std" => ""),

			array( "name" => __( 'Horizontal Center', ENGINE_THEME_NAME ),
					"desc" => __( 'Horizontally center background', ENGINE_THEME_NAME ),
					"id" => $shortname."_horizontal_center",
					"type" => "select",
					"options" => array("0", "1"),
					"std" => ""),

			array( "name" => __( 'Fit Always', ENGINE_THEME_NAME ),
					"desc" => __( 'Image will never exceed browser width or height Ignores min. dimensions', ENGINE_THEME_NAME ),
					"id" => $shortname."_fit_always",
					"type" => "select",
					"options" => array("0", "1"),
					"std" => ""),

			array( "name" => __( 'Fit Portrait', ENGINE_THEME_NAME ),
					"desc" => __( 'Portrait images will not exceed browser height', ENGINE_THEME_NAME ),
					"id" => $shortname."_fit_portrait",
					"type" => "select",
					"options" => array("0", "1"),
					"std" => ""),

			array( "name" => __( 'Fit Landscape', ENGINE_THEME_NAME ),
					"desc" => __( 'Landscape images will not exceed browser width', ENGINE_THEME_NAME ),
					"id" => $shortname."_fit_landscape",
					"type" => "select",
					"options" => array("0", "1"),
					"std" => ""),

			array( "type" => "close"),

			array( "name" => __( 'Translations', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-pencil"),
					
			array( "type" => "open"),

			array( "name" => __( 'Blog', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Blog Read More', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your label for blog continue reading', ENGINE_THEME_NAME ),
					"id" => $shortname."_continue_reading",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Comment Count Header', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header label for comment count box', ENGINE_THEME_NAME ),
					"id" => $shortname."_comment_cheader",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Comment Count Notification', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header label for comment count notification', ENGINE_THEME_NAME ),
					"id" => $shortname."_comment_notify",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Comment Form Header', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header label for comment form box', ENGINE_THEME_NAME ),
					"id" => $shortname."_comment_fheader",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Project', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Project Filter', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your project filter label', ENGINE_THEME_NAME ),
					"id" => $shortname."_project_cfilter",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Project Category', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your project category label', ENGINE_THEME_NAME ),
					"id" => $shortname."_pcategory_label",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Project Website', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your project website url', ENGINE_THEME_NAME ),
					"id" => $shortname."_vsite",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Other', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Member Header', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your member header label', ENGINE_THEME_NAME ),
					"id" => $shortname."_member_header",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Search Header', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header label for search page', ENGINE_THEME_NAME ),
					"id" => $shortname."_search_header",
					"type" => "text",
					"std" => ""),

			array( "name" => __( 'Search Input', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your search text input label', ENGINE_THEME_NAME ),
					"id" => $shortname."_search_label",
					"type" => "text",
					"std" => ""),

			array( "name" => __( '404 Top Label', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header label for 404 top label', ENGINE_THEME_NAME ),
					"id" => $shortname."_notfound_label",
					"type" => "text",
					"std" => ""),

			array( "name" => __( '404 Link', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your link label for 404 page not found', ENGINE_THEME_NAME ),
					"id" => $shortname."_notfound_link",
					"type" => "text",
					"std" => ""),

			array( "type" => "close"),

			array( "name" => __( 'Color Scheme', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-tint"),
					
			array( "type" => "open"),

			array( "name" => __( 'Menu Text', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter menu text color', ENGINE_THEME_NAME ),
					"id" => $shortname."_menu_text_color",
					"type" => "color",
					"std" => ""),	

			array( "name" => __( 'Menu Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter menu background color', ENGINE_THEME_NAME ),
					"id" => $shortname."_menu_bg_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Menu Background Hover/Active', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter menu background hover and active color', ENGINE_THEME_NAME ),
					"id" => $shortname."_menu_bg_hover",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Submenu Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter submenu background color', ENGINE_THEME_NAME ),
					"id" => $shortname."_submenu_bg_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Headers', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header text color', ENGINE_THEME_NAME ),
					"id" => $shortname."_header_text_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Headers Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your header background color', ENGINE_THEME_NAME ),
					"id" => $shortname."_header_bg_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Sidebar Headers', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your sidebar header text color', ENGINE_THEME_NAME ),
					"id" => $shortname."_sidebar_text_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Sidebar Headers Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your sidebar header background color', ENGINE_THEME_NAME ),
					"id" => $shortname."_sidebar_bg_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Slider Title Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your slider background title', ENGINE_THEME_NAME ),
					"id" => $shortname."_slider_title_bg",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Slider Title Text', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your slider title text color', ENGINE_THEME_NAME ),
					"id" => $shortname."_slider_text",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Button Background', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter button background color', ENGINE_THEME_NAME ),
					"id" => $shortname."_button_bg_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Button Background Text', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter button background text color', ENGINE_THEME_NAME ),
					"id" => $shortname."_button_text_color",
					"type" => "color",
					"std" => ""),

			array( "name" => __( 'Scrollbars', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your content custom scrollbar color', ENGINE_THEME_NAME ),
					"id" => $shortname."_scrollbar_color",
					"type" => "color",
					"std" => ""),

			array( "type" => "close"),

			array( "name" => __( 'Social', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-plus-square"),
			
			array( "type" => "open"),

			array( "name" => __( 'Facebook Username', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter Facebook username', ENGINE_THEME_NAME ),
					"id" => $shortname."_facebook",
					"type" => "text",
					"std" => ""),	

			array( "name" => __( 'Twitter Username', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter Twitter Username', ENGINE_THEME_NAME ),
					"id" => $shortname."_twitter",
					"type" => "text",
					"std" => ""),
					
			array( "type" => "close"),

			array( "name" => __( 'Theme Pages', ENGINE_THEME_NAME ), "type" => "top_section"),
			array( "type" => "close"),

			array( "name" => __( 'About', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-users"),

			array( "name" => __( 'Services', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Hide Services', ENGINE_THEME_NAME ),
					"desc" => __( 'check the box if you want to hide the services list', ENGINE_THEME_NAME ),
					"id" => $shortname."_services_hide",
					"type" => "checkbox",
					"std" => ""),	

			array( "name" => __( 'Number of Services', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter number of services you want to display', ENGINE_THEME_NAME ),
					"id" => $shortname."_num_services",
					"type" => "text",
					"std" => ""),	

			array( "name" => __( 'Services Ordering', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your services order type', ENGINE_THEME_NAME ),
					"id" => $shortname."_services_order",
					"type" => "select",
					"options" => array("ASC", "DESC"),
					"std" => ""),	

			array( "name" => __( 'Services Orderby', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your orderby parameter for services', ENGINE_THEME_NAME ),
					"id" => $shortname."_services_orderby",
					"type" => "select",
					"options" => array("ID", "author", "title", "name", "date", "modified", "parent", "rand", "comment_count", "menu_order"),
					"std" => ""),

			array( "name" => __( 'Members', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Hide Members', ENGINE_THEME_NAME ),
					"desc" => __( 'check the box if you want to hide the members list', ENGINE_THEME_NAME ),
					"id" => $shortname."_members_hide",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Number of Members', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter number of members you want to display in about page', ENGINE_THEME_NAME ),
					"id" => $shortname."_num_member",
					"type" => "text",
					"std" => ""),	

			array( "name" => __( 'Member Ordering', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your member order type', ENGINE_THEME_NAME ),
					"id" => $shortname."_member_order",
					"type" => "select",
					"options" => array("ASC", "DESC"),
					"std" => ""),	

			array( "name" => __( 'Member Orderby', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your orderby parameter for member', ENGINE_THEME_NAME ),
					"id" => $shortname."_member_orderby",
					"type" => "select",
					"options" => array("ID", "author", "title", "name", "date", "modified", "parent", "rand", "comment_count", "menu_order"),
					"std" => ""),

			array( "type" => "close"),

			array( "name" => __( 'Blog', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-calendar"),

			array( "name" => __( 'Main', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Layout Type', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your blog layout display type for single page only', ENGINE_THEME_NAME ),
					"id" => $shortname."_blog_sidebar_type",
					"type" => "select",
					"options" => array("Right", "Left"),
					"std" => ""),

			array( "name" => __( 'Number of Blog items', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter blog items you want to display in blog page', ENGINE_THEME_NAME ),
					"id" => $shortname."_num_blog",
					"type" => "text",
					"std" => ""),	

			array( "name" => __( 'Display Excerpt', ENGINE_THEME_NAME ),
					"desc" => __( 'Display your excerpt or brief description of your post', ENGINE_THEME_NAME ),
					"id" => $shortname."_display_excerpt",
					"type" => "checkbox",
					"std" => ""),	

			array( "name" => __( 'Blog Navigation', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your blog page navigation style', ENGINE_THEME_NAME ),
					"id" => $shortname."_blog_pnav",
					"type" => "select",
					"options" => array("Pagination", "Next Previous Link", "W|P|L|O|C|K|E|R|.|C|O|M"),
					"std" => ""),	

			array( "name" => __( 'Single Post', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Display Post Links', ENGINE_THEME_NAME ),
					"desc" => __( 'Check this box to display post links in single blog page', ENGINE_THEME_NAME ),
					"id" => $shortname."_display_posts",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Hide Tags', ENGINE_THEME_NAME ),
					"desc" => __( 'Hide tags in blog single page', ENGINE_THEME_NAME ),
					"id" => $shortname."_hide_tags",
					"type" => "checkbox",
					"std" => ""),	

			array( "type" => "close"),

			array( "name" => __( 'Projects', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-laptop"),

			array( "name" => __( 'Main', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Hide Categories', ENGINE_THEME_NAME ),
					"desc" => __( 'Hide project category dropdown', ENGINE_THEME_NAME ),
					"id" => $shortname."_hide_project_drop",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Project Hover Link', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your thumbnail hover link style', ENGINE_THEME_NAME ),
					"id" => $shortname."_project_hover_link",
					"type" => "select",
					"options" => array("Url", "Lightbox"),
					"std" => ""),	

			array( "name" => __( 'Project Ordering', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your project album order type', ENGINE_THEME_NAME ),
					"id" => $shortname."_album_order",
					"type" => "select",
					"options" => array("ASC", "DESC"),
					"std" => ""),	

			array( "name" => __( 'Project Orderby', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your orderby parameter for project album', ENGINE_THEME_NAME ),
					"id" => $shortname."_album_orderby",
					"type" => "select",
					"options" => array("ID", "author", "title", "name", "date", "modified", "parent", "rand", "comment_count", "menu_order"),
					"std" => ""),

			array( "name" => __( 'Single Post', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Thumbnail Slider', ENGINE_THEME_NAME ),
					"desc" => __( 'Checkbox to exclude the thumbnail in project slider', ENGINE_THEME_NAME ),
					"id" => $shortname."_project_thumb_slider",
					"type" => "checkbox",
					"std" => ""),

			array( "name" => __( 'Attachment Ordering', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your ordering style in project page for album images', ENGINE_THEME_NAME ),
					"id" => $shortname."_attachment_order",
					"type" => "select",
					"options" => array("ASC", "DESC"),
					"std" => ""),

			array( "name" => __( 'Attachment Orderby', ENGINE_THEME_NAME ),
					"desc" => __( 'Select your orderby parameter in project page for album images', ENGINE_THEME_NAME ),
					"id" => $shortname."_attachment_orderby",
					"type" => "select",
					"options" => array("ID", "author", "title", "name", "date", "modified", "parent", "rand", "comment_count", "menu_order"),
					"std" => ""),	

			array( "type" => "close"),

			array( "name" => __( 'Contact', ENGINE_THEME_NAME ),
					"type" => "section",
					"icon" => "fa fa-phone"),
			
			array( "type" => "open"),

			array( "name" => __( 'Google Map', ENGINE_THEME_NAME ), "type" => "headers"),

			array( "name" => __( 'Latitude', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your google map latitude', ENGINE_THEME_NAME ),
					"id" => $shortname."_map_lat",
					"type" => "text",
					"std" => "-12.043333"),

			array( "name" => __( 'Longitude', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your google map longitude', ENGINE_THEME_NAME ),
					"id" => $shortname."_map_lng",
					"type" => "text",
					"std" => "-77.028333"),

			array( "name" => __( 'Map Zoom', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your value for map zoom from 1-20', ENGINE_THEME_NAME ) ,
					"id" => $shortname."_map_zoom",
					"type" => "text",
					"std" => "8"),		

			array( "name" => __( 'Marker Title', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your google map marker title', ENGINE_THEME_NAME ),
					"id" => $shortname."_map_marker",
					"type" => "text",
					"std" => __( 'Add your marker title window', ENGINE_THEME_NAME )),	

			array( "name" => __( 'Marker Window', ENGINE_THEME_NAME ),
					"desc" => __( 'Enter your google map marker info window', ENGINE_THEME_NAME ),
					"id" => $shortname."_map_window",
					"type" => "textarea",
					"std" => __( 'Please add your text marker here', ENGINE_THEME_NAME )),	

			array( "type" => "close"),	
		);
	}

	add_action('init', 'arch_options_setup');
?>