<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content fullwidth">
			
			<!--PAGE NOT FOUND-->

			<?php
				$arch_pagenotfound_header = esc_attr( get_option('arch_pagenotfound_header') );
				$arch_notfound_link = esc_attr( get_option('arch_notfound_link') );
			?>

			<div class="page-not-found">
				<div class="nfound">
					<h2>
						<?php 
							if(!empty($arch_pagenotfound_header)) : echo $arch_pagenotfound_header; 
							else : _e( 'Error 404', ENGINE_THEME_NAME ); 
							endif; 
						?>
					</h2>
					<label>
						<?php 
							if(!empty($arch_notfound_link)) : ?>
								<a href="<?php echo home_url(); ?>"><?php echo $arch_notfound_link; ?></a><?php
							endif;
						?>
					</label>
				</div>
			</div>		

		</div>
	</div>

<?php get_footer(); ?>