<?php
/*
Template Name: Projects	
*/	
?>

<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content project-inside">
			<?php 
				get_theme_page_title(); 
				get_projects_category(); 
			?>
		</div>
	</div>

	<!--PROJECTS-->
	<div class="project-page load-item clear">
		<?php 
			$arch_project_order = esc_attr( get_option('arch_album_order') );
			$arch_project_orderby = esc_attr( get_option('arch_album_orderby') );

			if (have_posts()) : 
				while (have_posts()) : the_post(); 
					the_content(); 								
				endwhile; 
			endif;
		?>

		<!--PROJECT LIST-->
		<div class="horScroll">
			<section class="project-list clear">
				<?php
					$args = array( 
						'post_type' => 'projects', 
						'orderby' => $arch_project_orderby, 
						'order' => $arch_project_order, 
						'posts_per_page' => -1 
					);
					$wp_query = new WP_Query( $args );	

					while ($wp_query->have_posts()) : $wp_query->the_post(); 		
						get_template_part( 'loop', 'projects' );							
					endwhile;	
				?>	
			</section>
		</div>
	</div>

<?php get_footer(); ?>