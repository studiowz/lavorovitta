<?php
	$arch_exclude = esc_attr( get_post_meta( $post->ID, "exclude", true ) );
	$arch_video = esc_attr( get_post_meta( $post->ID, "video", true ) );
	$arch_project_hover_link = esc_attr( get_option('arch_project_hover_link') );
	$arch_project_categories = get_the_term_list( $post->ID, 'project_categories', ' ', ', ', '' );

	if($arch_project_hover_link == "Lightbox") :
		$arch_lightbox_class = "frame";
	endif;

	if(empty($arch_exclude)) : ?>
		<div id="post-<?php the_ID(); ?>" <?php sanitize_html_class( post_class() ); ?>>
			<div class="link-hover">
				<a href="<?php the_permalink(); ?>" class="<?php echo $arch_lightbox_class; ?>">
					<?php
						if ( post_password_required() ) : ?>
							<img src="<?php echo get_template_directory_uri(); ?>/img/password-protect.jpg" alt="" title="" /><?php
						else :
							if ( has_post_thumbnail() ) :
								the_post_thumbnail('portfolio-medium', array('title' => ''.get_the_title().''));
							else :
								?><img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.jpg" alt="" title="" /><?php
							endif;							
						endif;

						if (!empty($arch_video)) :
							?><div class="blog-hover"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/video.png" alt="" title="" /></div><?php
						else :
							?><div class="blog-hover"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/link.png" alt="" title="" /></div><?php
						endif;				
					?>
					
				</a>
			</div>
			<div class="project-details">
				<h3><a href="<?php the_permalink(); ?>" class="<?php echo $arch_lightbox_class; ?>"><?php the_title(); ?></a></h3>
				<span><?php echo $arch_project_categories; ?></span>				
			</div>
		</div><?php
	endif;
?>	