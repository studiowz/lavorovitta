<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content">
			
			<div class="content-inside clear">

				<?php
					$arch_blog_sidebar_type = esc_attr( get_option('arch_blog_sidebar_type') );
					$arch_blog_pnav = esc_attr( get_option('arch_blog_pnav') );

					if($arch_blog_sidebar_type == "Left") :
						$arch_blog_class_main = "left-content right";
						$arch_blog_class_sidebar = "sidebar left";
					else :
						$arch_blog_class_main = "left-content";
						$arch_blog_class_sidebar = "sidebar";
					endif;
				?>

				<!--LEFT CONTENT-->
				<div class="<?php echo $arch_blog_class_main; ?>">

					<!--BLOG LIST-->
					<section class="single-blog clear">
						<?php
							while (have_posts()) : the_post();
								if ( wp_attachment_is_image( $post->id ) ) : 
									$att_image = wp_get_attachment_image_src( $post->id, "blog-large"); 
									$description = $post->post_content;	?>

	                       	<img src="<?php echo $att_image[0];?>" class="attachment-medium" alt="<?php $post->post_excerpt; ?>" />
			                  <h2><?php the_title(); ?></h2>
									<div class="single-details">
										<ul>
											<li class="date"><?php the_time(get_option('date_format')); ?></li>
											<li class="user"><?php the_author_meta('display_name'); ?></li>
											<li class="bubble">
												<a href="<?php the_permalink(); ?>#comments">
													<?php comments_number('No Comments', '1 Comment', '% Comments'); ?>
												</a>
											</li>
										</ul>
									</div>
									<div class="single-contents"><?php echo $description; ?></div><?php 
								endif;
							endwhile;
						?>
					</section>

				</div>

				<!--SIDEBAR-->
				<div class="<?php echo $arch_blog_class_sidebar; ?>">
					<?php
						if($sidebar_choice == "Default") : get_sidebar( 'blog' );
						else : get_custom_sidebar(); 
						endif;
					?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>