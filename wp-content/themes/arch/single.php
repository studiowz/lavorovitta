<?php get_header(); ?>

	<div class="inside load-item">
		<div class="page-content">
			
			<div class="content-inside clear">

				<?php
					$arch_display_posts = esc_attr( get_option('arch_display_posts') );
					$arch_blog_sidebar_type = esc_attr( get_option('arch_blog_sidebar_type') );
					$arch_blog_pnav = esc_attr( get_option('arch_blog_pnav') );
					$arch_hide_tags = esc_attr( get_option('arch_hide_tags') );

					if($arch_blog_sidebar_type == "Left") :
						$arch_blog_class_main = "left-content right";
						$arch_blog_class_sidebar = "sidebar left";
					else :
						$arch_blog_class_main = "left-content";
						$arch_blog_class_sidebar = "sidebar";
					endif;
				?>

				<?php
					if(!empty($arch_display_posts)) : ?>
						<div class="post-link clear">
							<span class="prev"><?php previous_post_link('%link'); ?></span>
							<span class="next"><?php next_post_link('%link'); ?></span>
						</div><?php
					endif;
				?>

				<!--LEFT CONTENT-->
				<div class="<?php echo $arch_blog_class_main; ?>">

					<!--BLOG LIST-->
					<section class="single-blog clear">

						<?php
							if ( post_password_required() ) :
								?><div class="protected"><?php the_content(); ?></div><?php
							else :
								while (have_posts()) : the_post();
									$arch_video_host = esc_attr( get_post_meta( $post->ID, "video_host", true ) );
									$arch_video = esc_attr( get_post_meta( $post->ID, "video", true ) );
									$arch_large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'blog-large');
									
									if ( ( function_exists( 'get_post_format' ) && 'image' == get_post_format( $post->ID ) ) ) :
										if ( has_post_thumbnail() ) :
											the_post_thumbnail('blog-large', array('title' => ''.get_the_title().''));
										endif;
									elseif ( ( function_exists( 'get_post_format' ) && 'video' == get_post_format( $post->ID ) ) ) :
										if($arch_video_host == "self") : ?>
											<video id="videojs_blog" class="video-js vjs-default-skin" controls preload="none" width="606" height="350" poster="<?php echo $arch_large_image_url[0]; ?>" data-setup="{}">
											    <source src="<?php echo $arch_video; ?>" type='video/mp4' />
											    <source src="<?php echo $arch_video; ?>" type='video/webm' />
											    <source src="<?php echo $arch_video; ?>" type='video/ogg' />
											</video><?php
										else : ?>
											<iframe width="606" height="350" src="<?php echo $arch_video; ?>" frameborder="0" allowfullscreen></iframe><?php
										endif;
									elseif ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) ) ) : ?>
										<div class="blog-flexslider">
											<ul class="slides">
												<?php
													$args = array( 
														'post_type' => 'attachment', 
														'numberposts' => -1, 
														'post_status' => null, 
														'post_parent' => $post->ID 
													);
													$attachments = get_posts( $args );

													if ( $attachments ) :							
														foreach ( $attachments as $attachment ) :
															$attachment_id = $attachment->ID;
															$arch_large_image = wp_get_attachment_image( $attachment->ID, 'blog-large' );
															?><li><?php echo $arch_large_image; ?></li><?php
														endforeach;
													endif;
												?>
											</ul>
										</div><?php
									endif;
									?>
									<h2><?php the_title(); ?></h2>
									<div class="single-details">
										<ul>
											<li class="date"><?php the_time(get_option('date_format')); ?></li>
											<li class="user"><?php the_author_meta('display_name'); ?></li>
											<li class="bubble">
												<a href="<?php the_permalink(); ?>#comments">
													<?php comments_number('No Comments', '1 Comment', '% Comments'); ?>
												</a>
											</li>
										</ul>
									</div>
									<div class="single-contents"><?php the_content(); ?></div>

									<!--TAGS-->
									<?php
										if(empty($arch_hide_tags)) :
											?><div class="the-tags"><?php the_tags('', ' ', ''); ?></div><?php
										endif;
									
									comments_template(); 
								endwhile;
							endif;						
						?>
					</section>
				</div>

				<!--SIDEBAR-->
				<div class="<?php echo $arch_blog_class_sidebar; ?>">
					<?php
						if($sidebar_choice == "Default") : get_sidebar( 'blog' );
						else : get_custom_sidebar(); 
						endif;
					?>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>